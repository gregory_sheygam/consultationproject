package com.telran.testprojectforconsult;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.nsd.NsdManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.telran.testprojectforconsult.fragments.RegistrationFragment;
import com.telran.testprojectforconsult.models.ServerRequestStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements RegistrationFragment.RegistrationListener {


    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private boolean isRegistration = false;
    private FrameLayout fragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences sPref = getSharedPreferences("AUTH",MODE_PRIVATE);
        String token  = sPref.getString("TOKEN",null);
        if (token!=null){
            startMainActivity();
        }


        setContentView(R.layout.activity_login);

        getSupportActionBar().setTitle("Login");
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        fragmentContainer = (FrameLayout) findViewById(R.id.fragment_frame);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button loginBtn = (Button) findViewById(R.id.login_btn);
        loginBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        Button regBtn = (Button) findViewById(R.id.reg_btn);
        regBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showRegistrationFrag();
            }
        });
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void showRegistrationFrag() {
        isRegistration = true;
        RegistrationFragment fragment = new RegistrationFragment();
        fragment.setCallBackListener(this);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_frame,fragment,"REG_FRAG");
        transaction.commit();
        mLoginFormView.setVisibility(View.GONE);
        fragmentContainer.setVisibility(View.VISIBLE);
        getSupportActionBar().setTitle("Registration");

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public void onBackPressed() {
        if (isRegistration){
            RegistrationFragment fragment = (RegistrationFragment) getSupportFragmentManager().findFragmentByTag("REG_FRAG");
            if (fragment!=null){
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.remove(fragment);
                transaction.commit();
            }
            fragmentContainer.setVisibility(View.GONE);
            mLoginFormView.setVisibility(View.VISIBLE);
            isRegistration = false;
            getSupportActionBar().setTitle("Login");
        }else{
            super.onBackPressed();
        }

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            if (isRegistration) {
                fragmentContainer.setVisibility(show ? View.GONE : View.VISIBLE);
                fragmentContainer.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        fragmentContainer.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });

            }else{
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                        show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                    }
                });
            }

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            if (isRegistration)
                fragmentContainer.setVisibility(show ? View.GONE : View.VISIBLE);
            else
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);

        }
    }

    @Override
    public void registrationFragCallback(String login, String password) {
        showProgress(true);
        new UserLoginTask(login,password).execute();
    }
    private void startMainActivity() {
        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, ServerRequestStatus> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected ServerRequestStatus doInBackground(Void... params) {
            ServerRequestStatus status = new ServerRequestStatus();
            status.message = "64bcb198a1f697c616efefaa2046349d";

            //Imitation errors
            int rnd = new Random().nextInt(6);
            switch (rnd){
                case 0:
                    status.isError = true;
                    status.message = "User already exist!";
                    break;
                case 1:
                    status.isError = true;
                    status.message = "Server error! Call to support!";
                    break;
                case 2:
                    status.isError = true;
                    status.message = "Connection error, check your internet!";
                    break;

            }
            //Imitation server works
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (isRegistration){
                // TODO: 22.03.2017 Send request to server to method registration
            }else{
                // TODO: 22.03.2017 Send request to server to method login
            }


            return status;
        }

        @Override
        protected void onPostExecute(final ServerRequestStatus success) {
            mAuthTask = null;
            showProgress(false);

            if (!success.isError) {
                SharedPreferences sPref = getSharedPreferences("AUTH",MODE_PRIVATE);
                SharedPreferences.Editor editor = sPref.edit();
                editor.putString("TOKEN",success.message);
                editor.apply();
                startMainActivity();

            } else {
                AlertDialog dialog = new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("Server Error!")
                        .setMessage(success.message)
                        .setPositiveButton("Ok",null)
                        .create();
                dialog.show();

            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }


}

