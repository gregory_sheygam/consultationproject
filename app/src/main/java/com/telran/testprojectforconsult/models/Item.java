package com.telran.testprojectforconsult.models;

/**
 * Created by TelRan on 22.03.2017.
 */

public class Item {
    private String name, email;

    public Item() {
    }

    public Item(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
