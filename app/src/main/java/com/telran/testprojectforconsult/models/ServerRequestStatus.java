package com.telran.testprojectforconsult.models;

/**
 * Created by TelRan on 22.03.2017.
 */

public class ServerRequestStatus {
    public boolean isError = false;
    public String message = "";
}
