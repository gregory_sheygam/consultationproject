package com.telran.testprojectforconsult.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telran.testprojectforconsult.R;
import com.telran.testprojectforconsult.models.Item;

import java.util.ArrayList;

/**
 * Created by TelRan on 22.03.2017.
 */
public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.MyViewHolder> {
    private final Context context;
    private ArrayList<Item> items = new ArrayList();

    public MyListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item item = items.get(position);
        holder.nameTxt.setText(item.getName());
        holder.emailTxt.setText(item.getEmail());
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public Item getItem(int position){
        return items.get(position);
    }

    public void addItem(Item item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void addItemAtFront(Item item) {
        items.add(item);
        notifyItemInserted(0);
    }

    public void addItem(Item item, int position) {
        items.add(position, item);
        notifyItemInserted(position);
    }

    public void removeItem(Item item) {
        int position = items.indexOf(items);
        if (position > 0) {
            items.remove(item);
            notifyItemRemoved(position);
        }
    }

    public void removeItem(int position) {
        if (position > 0 && position < items.size()) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeItem(int startPosition, int count) {
        if (startPosition > 0 && startPosition < items.size() && startPosition + count < items.size()) {
            for (int i = startPosition; i < startPosition + count; i++) {
                items.remove(i);
            }
            notifyItemRangeRemoved(startPosition, count);
        }
    }

    public void updateItem(Item item, int position) {
        items.set(position, item);
        notifyItemChanged(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        Item item = items.remove(fromPosition);
        items.add(toPosition, item);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void clear() {

        notifyItemRangeRemoved(0, items.size());
        items.clear();

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nameTxt, emailTxt;
        public MyViewHolder(View itemView) {
            super(itemView);
            nameTxt = (TextView) itemView.findViewById(R.id.row_name_txt);
            emailTxt = (TextView) itemView.findViewById(R.id.row_email_txt);
        }
    }
}