package com.telran.testprojectforconsult.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.telran.testprojectforconsult.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ItemViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ItemViewFragment extends Fragment {

    private static final String EMAIL = "name";
    private static final String NAME = "email";

    private String name;
    private String email;

    private TextView nameTxt, emailTxt;


    public ItemViewFragment() {
        // Required empty public constructor
    }

    public static ItemViewFragment newInstance(String name, String email) {
        ItemViewFragment fragment = new ItemViewFragment();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putString(EMAIL, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME,"");
            email = getArguments().getString(EMAIL,"");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        nameTxt = (TextView) view.findViewById(R.id.item_view_name_txt);
        emailTxt = (TextView) view.findViewById(R.id.item_view_email_txt);
        nameTxt.setText(name);
        emailTxt.setText(email);
    }
}
