package com.telran.testprojectforconsult.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.telran.testprojectforconsult.R;
import com.telran.testprojectforconsult.models.Item;
import com.telran.testprojectforconsult.providers.HttpProvider;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddItemFragmentListener} interface
 * to handle interaction events.
 */
public class AddItemFragment extends Fragment {

    private EditText inputName, inputEmail;
    private FloatingActionButton saveBtn;
    private AddItemTask addItemTask;
    private ProgressBar addItemProgress;

    private AddItemFragmentListener mListener;

    public AddItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_item, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inputName = (EditText) view.findViewById(R.id.input_name);
        inputEmail = (EditText) view.findViewById(R.id.input_email);
        addItemProgress = (ProgressBar) view.findViewById(R.id.add_item_progress);
        saveBtn = (FloatingActionButton) view.findViewById(R.id.fab_save_item);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemTask = new AddItemTask();
                addItemTask.execute();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AddItemFragmentListener) {
            mListener = (AddItemFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement AddItemFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        addItemTask = null;
    }

    public interface AddItemFragmentListener {
        void callback();
    }

    private class AddItemTask extends AsyncTask<Void,Void,Void>{
        private String name, email;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            name = String.valueOf(inputName.getText());
            email = String.valueOf(inputEmail.getText());
            inputEmail.setVisibility(View.INVISIBLE);
            inputName.setVisibility(View.INVISIBLE);
            saveBtn.setVisibility(View.INVISIBLE);
            addItemProgress.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO: 26.03.2017 Get token from shared preferences
            // TODO: 26.03.2017 Checking if asynctask was stopped!
            Item item = new Item(name,email);
            HttpProvider.getInstance().addItem(item);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            inputEmail.setVisibility(View.VISIBLE);
            inputName.setVisibility(View.VISIBLE);
            saveBtn.setVisibility(View.VISIBLE);
            addItemProgress.setVisibility(View.INVISIBLE);
            if (mListener!=null){
                mListener.callback();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            addItemTask = null;
            inputEmail.setVisibility(View.VISIBLE);
            inputName.setVisibility(View.VISIBLE);
            saveBtn.setVisibility(View.VISIBLE);
            addItemProgress.setVisibility(View.INVISIBLE);
        }
    }
}
