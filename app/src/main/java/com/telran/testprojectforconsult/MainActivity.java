package com.telran.testprojectforconsult;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.telran.testprojectforconsult.fragments.AddItemFragment;
import com.telran.testprojectforconsult.fragments.GalleryFragment;
import com.telran.testprojectforconsult.fragments.ItemViewFragment;
import com.telran.testprojectforconsult.fragments.MyListFragment;
import com.telran.testprojectforconsult.fragments.SlideshowFragment;
import com.telran.testprojectforconsult.fragments.ToolsFragment;
import com.telran.testprojectforconsult.models.Item;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AddItemFragment.AddItemFragmentListener {

    private FragmentManager manager;
    private FragmentTransaction transaction;
    private DrawerLayout drawer;
    private boolean isDrawerLocked = false;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = getSupportFragmentManager();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("My list");
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_action_bar_home);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDrawerLocked){
                    drawerStateSwitcher();
                }else{
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        showListFragment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(isDrawerLocked){
            drawerStateSwitcher();
        }else if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_list) {
            showListFragment();
        } else if (id == R.id.nav_gallery) {
            transaction = manager.beginTransaction();
            transaction.replace(R.id.activity_main_container,new GalleryFragment());
            transaction.commit();
            toolbar.setTitle("Gallery");

        } else if (id == R.id.nav_slideshow) {
            transaction = manager.beginTransaction();
            transaction.replace(R.id.activity_main_container,new SlideshowFragment());
            transaction.commit();
            toolbar.setTitle("Slideshow");

        } else if (id == R.id.nav_manage) {
            transaction = manager.beginTransaction();
            transaction.replace(R.id.activity_main_container,new ToolsFragment());
            transaction.commit();
            toolbar.setTitle("Tools");

        } else if (id == R.id.nav_logout) {
            SharedPreferences sPref = getSharedPreferences("AUTH", MODE_PRIVATE);
            SharedPreferences.Editor editor = sPref.edit();
            editor.clear();
            editor.commit();
            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showListFragment(){
        MyListFragment listFragment = new MyListFragment();
        listFragment.setFragmentListener(new MyListFragment.ListFragmentListener() {
            @Override
            public void itemSelected(Item item) {
                showItemViewFragment(item);
                drawerStateSwitcher();

            }

            @Override
            public void addItem() {
                showAddItemFragment();
                drawerStateSwitcher();
            }
        });

        transaction = manager.beginTransaction();
        transaction.replace(R.id.activity_main_container,listFragment);
        transaction.commit();
        toolbar.setTitle("My list");
    }

    private void drawerStateSwitcher(){
        if(isDrawerLocked){
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toolbar.setNavigationIcon(R.drawable.ic_action_bar_home);
            isDrawerLocked = false;
            manager.popBackStack();
        }else{
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toolbar.setNavigationIcon(R.drawable.ic_action_bar_back);
            isDrawerLocked = true;
        }
    }

    private void showAddItemFragment(){
        AddItemFragment addItemFragment = new AddItemFragment();
        addItemFragment.onAttach((Context) this);
        transaction = manager.beginTransaction();
        transaction.replace(R.id.activity_main_container,addItemFragment);
        transaction.addToBackStack("REPLACE_TO_ADD_ITEM");
        transaction.commit();
    }

    private void showItemViewFragment(Item item){
        ItemViewFragment fragment = ItemViewFragment.newInstance(item.getName(),item.getEmail());
        transaction = manager.beginTransaction();
        transaction.replace(R.id.activity_main_container,fragment);
        transaction.addToBackStack("REPLACE_TO_ITEM_VIEW");
        transaction.commit();
    }

    @Override
    public void callback() {
        drawerStateSwitcher();
    }
}
