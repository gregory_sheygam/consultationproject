package com.telran.testprojectforconsult.providers;

import com.telran.testprojectforconsult.models.Item;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by TelRan on 26.03.2017.
 */

public class HttpProvider {
    ArrayList<Item> items = new ArrayList<>();
    private static final HttpProvider ourInstance = new HttpProvider();

    public static HttpProvider getInstance() {
        return ourInstance;
    }

    private HttpProvider() {
    }

    public ArrayList<Item> getList() {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return items;
    }

    public void addItem(Item item){
        items.add(item);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
